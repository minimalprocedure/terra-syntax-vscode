#  terra-syntax-vscode

Really simple syntax highlighting for the [Terra](https://github.com/terralang/terra) programming language.

## note
- Syntax is adapted from [Terra-VSCode](https://github.com/DvvCz/Terra-VSCode) based on [VSCode-Emmylua](https://github.com/EmmyLua/VSCode-EmmyLua).
- Logo from the [terralang](https://terralang.org) website